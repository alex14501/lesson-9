#include "functions.h"
#include <iostream>


int main() {
	typedef temp<int> check;
	check test(10);
	if (test==10)
	{
		std::cout << "equal good" << std::endl;
	}
	if (test < 11)
	{
		std::cout << "smaller good"<<std::endl;
	}
	if (test > 9)
	{
		std::cout << "bigger good" << std::endl;
	}
	//check compare
	std::cout << "correct print is 1 -1 0 1" << std::endl;
	std::cout << compare<double>(1.0, 2.5) << std::endl;
	std::cout << compare<double>(4.5, 2.4) << std::endl;
	std::cout << compare<double>(4.4, 4.4) << std::endl;
	std::cout << compare<double>('a', 'b') << std::endl;
	//check bubbleSort
	std::cout << "correct print is sorted array" << std::endl;

	const int arr_size = 5;
	double doubleArr[arr_size] = { 1000.0, 2.0, 3.4, 17.0, 50.0 };
	bubbleSort<double>(doubleArr, arr_size);
	for (int i = 0; i < arr_size; i++) {
		std::cout << doubleArr[i] << " ";
	}
	std::cout << std::endl;

	//check printArray
	std::cout << "correct print is sorted array" << std::endl;
	printArray<double>(doubleArr, arr_size);
	std::cout << std::endl;

	char charArr[arr_size] = { 'b', 'a', 'c', 'e', 'd' };
	bubbleSort<char>(charArr, arr_size);
	for (int i = 0; i < arr_size; i++) {
		std::cout << charArr[i] << " ";
	}
	std::cout << std::endl;
	printArray<char>(charArr, arr_size);
	std::cout << std::endl;
	system("pause");
	return 1;
}