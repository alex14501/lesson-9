#include <iostream>
#include "BSNode.h"
BSNode::BSNode(string data)
{
	this->_data = data;
	this->_left = nullptr;
	this->_right = nullptr;
	this->_count = 1;
}
BSNode::BSNode(const BSNode& other)
{
	this->_data = other.getData();
	this->_left = other.getLeft();
	this->_right = other.getRight();
}

BSNode::~BSNode()
{
	delete this->_left;
	delete this->_right;
	
}
void BSNode::insert(string value)
{
	if (value < this->_data)
	{
		if (this->_left != nullptr)
		{
			this->_left->insert(value);
		}
		else
		{
			this->_left = new BSNode(value);
		}
	}
	else
	{
		if (value > this->_data)
		{
			if (this->_right != nullptr)
			{
				this->_right->insert(value);
			}
			else
			{
				this->_right = new BSNode(value);
			}
		}
		else
		{
			this->_count++;
		}
	}
}
BSNode& BSNode::operator=(const BSNode& other)
{
	BSNode* newNode = new BSNode(other);
	return *newNode;
}

bool BSNode::isLeaf() const
{
	if (this->_left == nullptr&&this->_right == nullptr)
	{
		return true;
	}
	else
	{
		return false;
	}
}
string BSNode::getData() const
{
	return this->_data;
}
BSNode* BSNode::getLeft() const
{
	return this->_left;
}
BSNode* BSNode::getRight() const
{
	return this->_right;
}

bool BSNode::search(string val) const
{
	bool trueOrFalse = false;
	if (this->_data == val)
	{
		return true;
	}
	else
	{
		if (this->_left == nullptr&&this->_right == nullptr)
		{
			return false;
		}
		else
		{
			if (this->_right != nullptr)
			{
				trueOrFalse = this->_right->search(val);
			}
			if (this->_left != nullptr)
			{
				if (!trueOrFalse)
				{
					trueOrFalse = this->_right->search(val);
				}
			}
			return trueOrFalse;
		}
	}
}

int BSNode::getHeight() const
{
	int height = 0;
	int tempHeight = 0;
	if (this->_left != nullptr||this->_right != nullptr)
	{
		if (this->_left != nullptr)
		{
			tempHeight = this->_left->getHeight();
			if (tempHeight > height)
			{
				height = tempHeight;
			}
		}
		if (this->_right != nullptr)
		{
			tempHeight = this->_right->getHeight();
			if (tempHeight > height)
			{
				height = tempHeight;
			}
		}
		return height + 1;
	}
	else
	{
		return height + 1;
	}
}
int BSNode::getDepth(const BSNode& root) const
{
	int depth = 0;
	BSNode* tempNode = new BSNode(root);
	while (this->_data != tempNode->getData())
	{
		if (this->_data > tempNode->getData())
		{
			tempNode = tempNode->getRight();
		}
		else
		{
			if (this->_data < tempNode->getData())
			{
				tempNode = tempNode->getLeft();
			}
		}
		depth++;
	}
	return depth;
}

void BSNode::printNodes() const
{
	if (this->getLeft() != nullptr)
	{
		this->getLeft()->printNodes();
	}
	std::cout << this->_data << endl;
	if (this->getRight() != nullptr)
	{
		this->getRight()->printNodes();
	}
	//std::cout << this->_data << endl;
}//for question 1 part C
//int BSNode::getCurrNodeDistFromInputNode(const BSNode* node) const
//{
//
//}